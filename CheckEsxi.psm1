class CheckEsxi{
    [String]$name
    [String]$desiredValue
    [String]$value
    $check

    CheckEsxi([String]$name,$desiredValue){
        $this.name = $name
        $this.desiredValue = $desiredValue
    }
    obtainValue($VMWareObject){
        throw("Must Override Method")
    }
    setValue($VMWareObject, $objectName){
        throw("Must Override Method")
    }
    [String]isSuccess(){
        return $(if ($this.value -eq $this.desiredValue) {'OK'} else {'Error'})
    }
}

class CheckAdvSettingEsxi : CheckEsxi{
    [String]$functionParameter

    CheckAdvSettingEsxi([String]$name, [String]$desiredValue, [String]$functionParameter) :
    base([String]$name, [String]$desiredValue){
        $this.functionParameter =  $functionParameter
    }
    obtainValue($VMHost){
        $this.check = (Get-AdvancedSetting $this.functionParameter -Entity $VMHost)
        $this.value = $this.check.value
    }
    setValue($VMHost){
        $this.obtainValue($VMHost)
        if (([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if ((Read-Host "Do you want to change $($this.name) current value from host $($VMHost.name) to $($this.desiredValue) (y/n)") -eq 'y' ){ 
                $this.check | Set-AdvancedSetting -Value $this.desiredValue | out-null
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }
    }
}

class CheckServiceEsxi : CheckEsxi{
    [String]$serviceName

    CheckServiceEsxi([String]$name, $desiredValue, [String]$serviceName) : 
    base([String]$name, $desiredValue){
        $this.serviceName = $serviceName
    }
    obtainValue($VMHost){
        $this.check = Get-VMHostService -VMHost $VMHost | ? {$_.Key -eq  $this.serviceName}
        $this.value = "Policy: $($this.check.Policy), Running: $($this.check.Running)"
    }
    setValue($VMHost){
        $this.obtainValue($VMHost)
        if(([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if((Read-Host "Do you want to $($this.name) from host $($VMHost.name) (y/n)") -eq 'y' ){
                $this.check | Stop-VMHostService | out-null
                $this.check | Set-VMHostService -Policy off | out-null
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow}  
        } 
    }
    [String]isSuccess()
    {
        return $(if ($this.desiredValue.split("/") -contains $this.value) {"Ok"} else {"Error"})
    }
}

class CheckAdEsxi : CheckEsxi{
    [String]$exampleValue
    
    CheckAdEsxi([String]$name, [String]$desiredValue, [String]$exampleValue) :
    base([String]$name, [String]$desiredValue){
        $this.exampleValue = $exampleValue
    }
    setDesiredValue(){
        Write-Host "Choose desired value for $($this.name) check (Input Example: $($this.exampleValue))" -ForegroundColor Yellow
        $this.desiredValue = Read-Host
    }
    obtainValue($VMHost){
        $this.check = Get-VMHostAuthentication -VMHost $VMHost
        $this.value = "Domain: $($this.check.Domain), DomainMembershipStatus: $($this.check.DomainMembershipStatus)"
    }
    setValue($VMHost){
        $this.obtainValue($VMHost)
        if(([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if ((Read-host "Current value differs from desire value, Do you want to set domain for $($VMHost.name) (y/n)" ) -eq "y"){
                $domain = Read-Host "Enter Domain"
                $credential = Get-Credential -Message "Enter Username and Password for Domain $domain"
                $this.check | Set-VMHostAuthentication -Domain $domain -Credential $credential -JoinDomain            
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow}  
        }
    }
}

class CheckLockdownExceptionUsersEsxi : CheckEsxi{
    [String]$exampleValue

    CheckLockdownExceptionUsersEsxi([String]$name, [String]$desiredValue, [String]$exampleValue) :
    base([String]$name, [String]$desiredValue){
        $this.exampleValue = $exampleValue
    }
    setDesiredValue(){
        Write-Host "Choose desired value for $($this.name) check (Input Example: $($this.exampleValue))" -ForegroundColor Yellow
        $this.desiredValue = Read-Host
    }
    obtainValue($VMHost){
        $this.check = (Get-View -Id (Get-VMHost -Name $VMHost | Get-View).ConfigManager.HostAccessManager)
        $this.value = "$(($this.check.QueryLockdownExceptions()) -join ',')"
    }
    setValue($VMHost){
        $this.obtainValue($VMHost)
        if(([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if((Read-Host "Current value differs from desire value, Do you want to add users to $($this.name) for host $($VMHost.name) (y/n)") -eq "y"){
                if ((Read-Host "Are you sure you want to add $($this.desiredValue) (y/n)") -eq "y"){
                    $this.check.UpdateLockdownExceptions($($this.desiredValue).split(','))
                }
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow} 
        }        
    }
    [String]isSuccess(){
        $parseValue = ($this.value -replace '\s','').split(',')
        $parseDesiredValue = ($this.desiredValue -replace '\s','').split(',')

        return $(if($parseValue.count -eq $parseDesiredValue.count -and (($parseValue | ? {$parseDesiredValue -notcontains $_}).count -eq 0)){'OK'} else {"Error"})
    }
}

class CheckLockdownModeEsxi : CheckEsxi{
    CheckLockdownModeEsxi([String]$name, [String]$desiredValue) :
    base([String]$name, [String]$desiredValue){}
    obtainValue($VMHost){
        $this.value = (Get-VMHost -Name $VMHost | Get-View).Config.LockdownMode
    }
    setValue($VMHost){
        $this.obtainValue($VMHost)
        if(([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if((Read-Host "Do You want to change $($this.name) from host $($VMHost.name) to normal (y/n)") -eq "y"){
                if ((Read-Host "Are you sure you want to change $($this.name) to normal (y/n)") -eq "y"){
                    (Get-View -Id (Get-VMHost $VMHost | Get-View).ConfigManager.HostAccessManager).ChangeLockdownMode('lockdownNormal')
                }
                else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }
    }
}

class CheckAdvSettingEsxiCustom : CheckAdvSettingEsxi{
    [String]$exampleValue

    CheckAdvSettingEsxiCustom([String]$name, [String]$desiredValue, [String]$functionParameter, [String]$exampleValue) :
    base([String]$name, [String]$desiredValue, [String]$functionParameter){
        $this.exampleValue = $exampleValue
    }
    setDesiredValue(){
        Write-Host "Choose desired value for $($this.name) check (Input Example: $($this.exampleValue))" -ForegroundColor Yellow
        $this.desiredValue = Read-Host
    }
    setValue($VMHost){
        ([CheckAdvSettingEsxi]$this).obtainValue($VMHost)
        if(([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if((Read-Host "Current value differs from desire value, Do you want to configure value for $($this.name) for host $($VMHost.name) (y/n)") -eq "y"){
                ([CheckAdvSettingEsxi]$this).setValue($VMHost)
            }
            else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }    
    }
    [String]isSuccess(){
        $parseValue = ($this.value -replace '\s','').split(',')
        $parseDesiredValue = ($this.desiredValue -replace '\s','').split(',')

        return $(if (([CheckEsxi]$this).isSuccess() -eq "OK" -or 
        ($parseValue.count -eq $parseDesiredValue.count -and (($parseValue | ? {$parseDesiredValue -notcontains $_}).count -eq 0))){'OK'} else {"Error"})
    }
}
class NetworkCheck : CheckEsxi{
    NetworkCheck ([String]$name, [String]$desiredValue) :
    base([String]$name, [String]$desiredValue){}

    obtainValue($networkObject){
        $this.check = ($networkObject | Get-SecurityPolicy)
    }
}
class CheckForgedTransmits : NetworkCheck{
    CheckForgedTransmits([String]$name, [String]$desiredValue) :
    base([String]$name, [String]$desiredValue){}

    obtainValue($networkObject){
        ([NetworkCheck]$this).obtainValue($networkObject)
        $this.value = $this.check.ForgedTransmits
    }
    setValue($networkObject, $objectName){
        $this.obtainValue($networkObject)
        if (([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if ((Read-Host "Do you want to set $($this.name) value to $($this.desiredValue) for $objectName (y/n)") -eq "y"){

                $this.check | Set-SecurityPolicy -ForgedTransmits ([bool]::Parse($this.desiredValue)) | Out-null
            }else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }  
    }
}

class CheckMacChanges : NetworkCheck{
    CheckMacChanges([String]$name, [String]$desiredValue) :
    base([String]$name, [String]$desiredValue){}

    obtainValue($networkObject){
        ([NetworkCheck]$this).obtainValue($networkObject)
        $this.value =  $this.check.MacChanges
    }
    setValue($networkObject, $objectName){
        $this.obtainValue($networkObject)
        if (([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if ((Read-Host "Do you want to set $($this.name) value to $($this.desiredValue) for $objectName (y/n)") -eq "y"){

                $this.check | Set-SecurityPolicy -MacChanges ([bool]::Parse($this.desiredValue)) | Out-null
            }else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }  
    }
}

class CheckPromiscuosMode : NetworkCheck{
    CheckPromiscuosMode([String]$name, [String]$desiredValue) :
    base([String]$name, [String]$desiredValue){}

    obtainValue($networkObject){
        ([NetworkCheck]$this).obtainValue($networkObject)
        $this.value = $this.check.AllowPromiscuous
    }
    setValue($networkObject, $objectName){
        $this.obtainValue($networkObject)
        if (([CheckEsxi]$this).isSuccess() -eq "ERROR"){
            if ((Read-Host "Do you want to set $($this.name) value to $($this.desiredValue) for $objectName (y/n)") -eq "y"){

                $this.check | Set-SecurityPolicy -AllowPromiscuous ([bool]::Parse($this.desiredValue)) | Out-null
            }else{Write-Host "Operation cancelled" -ForegroundColor Yellow}
        }  
    }
}
