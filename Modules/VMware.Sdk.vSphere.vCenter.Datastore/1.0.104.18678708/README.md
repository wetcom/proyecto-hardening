# VMware.Sdk.vSphere.vCenter.Datastore

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

This **PowerShell** module is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project. The [PowerShell Mustache templates](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator/src/main/resources/powershell) are specifically modified by **VMware** to produce the module.

- API version: v7.0U3
- SDK version: 1.0.0

<a name="frameworks-supported"></a>
## Frameworks supported
- PowerShell **5.1 or 7.0**

<a name="installation"></a>
## Installation

**VMware.Sdk.vSphere.vCenter.Datastore** module is part of the [VMware.PowerCLI](https://www.powershellgallery.com/packages/VMware.PowerCLI) module. To install **VMware.Sdk.vSphere.vCenter.Datastore** run the following command:

```powershell
Install-Module -Name 'VMware.Sdk.vSphere.vCenter.Datastore'
```
