Using module '.\CheckEsxi.psm1'
Using module '.\config.ps1'
. .\config.ps1

Function Connect-vCenter{
    try {
        connect-viServer -ErrorAction Stop
    }
    catch {
        Write-host "Failed to connect to vCenter" -ForegroundColor Red
        break
    }
}
Function Set-DesiredValue{
    foreach($check in $checkListEsxi){
        if ($customizableClases -contains $check.GetType())
        {
            $check.setDesiredValue()
        }
    }
}

Connect-vCenter
Set-DesiredValue

$vHosts = Get-VMHost
$vSwitches = Get-VirtualSwitch | ? {$_.ExtensionData.gettype() -ne [VMware.Vim.DistributedVirtualSwitch]} 

foreach($vHost in $vHosts){
    foreach($check in $checkListEsxi){
        $check.setValue($vHost)
    }
}

foreach($vSwitch in $vSwitches){
    foreach($check in $checkListNetwork){
        $check.setValue($vSwitch, "vSwitch: $($vSwitch.name)")
    }
    $portgroups = $vSwitch | Get-VirtualPortGroup
    foreach($portgroup in $portgroups){
        foreach($check in $checkListNetwork){
            $check.setValue($portgroup, "vSwitch: $($vSwitch.name), Portgroup: $($portgroup.name)")
        }
    }
}
Disconnect-VIServer -Server * -Confirm:$false -Force:$true;