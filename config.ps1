Using module '.\CheckEsxi.psm1'

Import-Module .\Modules\VMware.VimAutomation.Sdk
Import-Module .\Modules\VMware.VimAutomation.Common
Import-Module .\Modules\Vmware.Vim
Import-Module .\Modules\VMware.VimAutomation.Cis.Core
Import-Module .\Modules\VMware.VimAutomation.Core
Import-Module .\Modules\VMware.VimAutomation.Storage
Import-Module .\Modules\VMware.VimAutomation.Vds
Import-Module .\Modules\VMware.VimAutomation.Sdk

Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -confirm:$false | Out-null
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -confirm:$false | Out-null

$valorDeseadoServices = "Policy: off, Running: False/Policy: on, Running: True"
$checkListEsxi = @(
    [CheckAdvSettingEsxi]::new("account-auto-unlock-time",900,"Security.AccountUnlockTime"),
    [CheckAdvSettingEsxi]::new("account-lockout",3,"Security.AccountLockFailures"),
    [CheckAdvSettingEsxi]::new("account-Password-history",5,"Security.PasswordHistory"),
    [CheckAdvSettingEsxiCustom]::new("account-password-policies","-","Security.PasswordQualityControl","retry=3 min=disabled,disabled,disabled,7,7"),
    [CheckAdEsxi]::new("ad-enabled","-","Domain: domain.net, DomainMembershipStatus: Ok"),
    [CheckAdvSettingEsxiCustom]::new("CPU-hyperthread-warning","-","UserVars.SuppressHyperthreadWarning",0),
    [CheckAdvSettingEsxi]::new("dcui-timeout",600,"UserVars.DcuiTimeOut"),
    [CheckServiceEsxi]::new("disable-cim",$valorDeseadoServices,"sfcbd-watchdog"),
    [CheckAdvSettingEsxi]::new("disable-mob",$false,"Config.HostAgent.plugins.solo.enableMob"),
    [CheckServiceEsxi]::new("disable-slp",$valorDeseadoServices,"slpd"),
    [CheckServiceEsxi]::new("disable-snmp",$valorDeseadoServices,"snmpd"),
    [CheckServiceEsxi]::new("disable-ssh",$valorDeseadoServices,"TSM-SSH"),
    [CheckAdvSettingEsxiCustom]::new("lockdown-dcui-access","-","DCUI.Access","root,admin"),
    [CheckLockdownExceptionUsersEsxi]::new("lockdown-exception-users","-","root,juan"),
    [CheckLockdownModeEsxi]::new("lockdown-mode","lockdownNormal"),
    [CheckAdvSettingEsxi]::new("logs-level","info","Config.HostAgent.Log.level"),
    [CheckAdvSettingEsxi]::new("network-bpdu",1,"Net.BlockGuestBPDU")
    [CheckServiceEsxi]::new("disable-shell",$valorDeseadoServices,"TSM"),
    [CheckAdvSettingEsxi]::new("tls-protocols","sslv3,tlsv1,tlsv1.1","UserVars.ESXiVPsDisabledProtocols"),
    [CheckAdvSettingEsxi]::new("transparent-page-sharing",2,"Mem.ShareForceSalting"),
    [CheckAdvSettingEsxi]::new("vib-trusted-binaries",$true,"VMkernel.Boot.execInstalledOnly")

)
$checkListNetwork = @(
    [CheckForgedTransmits]::new("network-reject-forged-transmit-standarswitch",$false),
    [CheckMacChanges]::new("network-reject-mac-changes-standarswitch",$false),
    [CheckPromiscuosMode]::new("network-reject-promiscuous-mode-standardswitch",$false)
)

$customizableClases = @([CheckAdEsxi],[CheckLockdownExceptionUsersEsxi],[CheckAdvSettingEsxiCustom])