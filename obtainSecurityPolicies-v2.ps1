Using module '.\CheckEsxi.psm1'
Using module '.\config.ps1'
Using module '.\ui.ps1'
. .\config.ps1
. .\ui.ps1

Function Connect-vCenter{
    try {
        connect-viServer -ErrorAction Stop
    }
    catch {
        Write-host "Failed to connect to vCenter" -ForegroundColor Red
        break
    }
}

Function Set-DesiredValue{
    foreach($check in $checkListEsxi){
        if ($customizableClases -contains $check.GetType())
        {
            $check.setDesiredValue()
        }
    }
}

$reportEsxi = @()
$reportVSwitch = @()
$reportPG = @()

Connect-vCenter
Set-DesiredValue

$vHosts = Get-VMHost
$vSwitches = Get-VirtualSwitch | ? {$_.ExtensionData.gettype() -ne [VMware.Vim.DistributedVirtualSwitch]} 

Write-Host -NoNewline "Processing ESXi... "
for ($i = 0; $i -lt $vHosts.count; $i++){
    Show-Progress $vHosts.count ($i + 1)
    foreach($check in $checkListEsxi)
    {
        $check.obtainValue($vHosts[$i])
        $reportEsxi += [PSCustomObject]@{
            "vCenter" = $vHosts[$i].Uid.split('@')[1].split('/')[0].split(':')[0]
            "Host" = $vHosts[$i].name;
            "Chequeo" = $check.name;
            "Valor Actual" =  $check.value;
            "Valor Deseado" = $check.desiredValue;
            "Resultado" = $check.isSuccess()
        }              
    }
}
Write-Host -NoNewline "Processing vDS... "
for($i = 0; $i -lt $vSwitches.count; $i++){
    Show-Progress $vSwitches.count ($i + 1)
    foreach($check in $checkListNetwork)
    {
        $check.obtainValue($vSwitches[$i])
        $reportVSwitch += [PSCustomObject]@{
            "vCenter" = $vSwitches[$i].Uid.split('@')[1].split('/')[0].split(':')[0]
            "Host" = $vSwitches[$i].VmHost;
            "vSwitch" = $vSwitches[$i].Name;
            "Chequeo" = $check.name;
            "Valor Actual" =  $check.value;
            "Valor Deseado" = $check.desiredValue;
            "Resultado" = $check.isSuccess()
        }        
    }
    $portgroups = $vSwitches[$i] | Get-VirtualPortGroup
        foreach ($portgroup in $portgroups){
            foreach($check in $checkListNetwork)
            {
                $check.obtainValue($vSwitches[$i])
                $reportPG += [PSCustomObject]@{
                    "vCenter" = $vSwitches[$i].Uid.split('@')[1].split('/')[0].split(':')[0]
                    "Host" = $vSwitches[$i].VmHost;
                    "vSwitch" = $vSwitches[$i].Name;
                    "PG " = $portgroup.Name;
                    "Chequeo" = $check.name;
                    "Valor Actual" = $check.value;
                    "Valor Deseado" = $check.desiredValue;
                    "Resultado" = $check.isSuccess()
                }
            }
        }
}
$reportEsxi | export-csv '.\Reportes\securityPoliciesESXi.csv' -NoTypeInformation
$reportVSwitch | export-csv '.\Reportes\securityPoliciesvSwitch.csv' -NoTypeInformation
$reportPG | export-csv '.\Reportes\securityPoliciesPG.csv' -NoTypeInformation
Disconnect-VIServer -Server * -Confirm:$false -Force:$true;