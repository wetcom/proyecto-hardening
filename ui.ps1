function Show-Progress($total, $index){
	$current = [math]::Round(($index / $total) * 100)
	$whitespace = 3 - $current.ToString().Length
	Write-Host (" " * $whitespace  + $current + "%") -NoNewline
    
    if ($current -lt 100) {
        Write-Host "`b`b`b`b" -NoNewline
    } else {
        Write-Host ""
    }
}